pragma solidity ^0.4.0;
contract TicTacToe {
    uint[] board = new uint[](9);
    address player1;
    address player2;
    uint startingPlayer = 0;
    uint playerNum = 0;
    uint alternator = 0;
    uint winner = 0;
    uint start = 0;
    
    event EndTurn(address playerAddress);
    event JoinGame(uint status);

    function joinGame(address sender)public {
        if(winner != 0){
            reset();
        }
        if(playerNum == 0){
           player1 = sender; 
           playerNum  = playerNum + 1;
           JoinGame(0);
       }else if(playerNum == 1){
            player2 = sender;
            playerNum = playerNum + 2;
            JoinGame(0);
       }else{
           JoinGame(1);
       }
    }

    function reset() public{
        board = new uint[](9);
        player1 = 0;
        player2 = 0;
        playerNum = 0;
        winner = 0;
        alternator = alternator + 1;
        start = alternator % 2;
        EndTurn(0);
    }
    
    function doMove(uint place) public returns (uint){
        
        //checks for correct users and correct move
        if(playerNum < 2){
            return 1;
        }
        if(start == 0){
            if(msg.sender != player1) return 1;
        }else if(start == 1){
            if(msg.sender != player2) return 1;
        }      
        if(place < 0 || place >= 9){
            return 1;
        }
        if(board[place] != 0){
           return 1; 
        }
        
        board[place] = start+1;
        start = (start + 1) % 2;

        winner = checkWinner();
        
        //generate event
        EndTurn(msg.sender);
        
        return 0;   
    }
    

    
    uint[][]  tests = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]];
 // 0 1 2
// 3 4 5
// 6 7 8
function checkWinner() public constant returns (uint){
    for(uint i =0; i < tests.length;i++){
        uint[] memory currentTest = tests[i];
        if(board[currentTest[0]] == board[currentTest[1]] && board[currentTest[1]] == board[currentTest[2]] && board[currentTest[0]] != 0){
          return board[currentTest[0]];  
        }
    }

    
    return 0;
}
    
    function getStatus() public constant returns(uint, uint[]) {
        uint statusCode;
        if(playerNum < 2){
            statusCode = 3;
        }else{
            statusCode = winner;
        }
        
        return (statusCode, board);
    }

}