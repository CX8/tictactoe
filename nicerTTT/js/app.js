App = {
  web3Provider: null,
  p1: null,
  p2: null,
  contract: null,
  contractABI : [{"constant":false,"inputs":[{"name":"sender","type":"address"}],"name":"joinGame","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getStatus","outputs":[{"name":"","type":"uint256"},{"name":"","type":"uint256[]"},{"name":"","type":"address[]"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"checkWinner","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"place","type":"uint256"}],"name":"doMove","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"switchPlayers","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"reset","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"playerAddress","type":"address"}],"name":"endTurn","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"accepted","type":"uint256"}],"name":"playerJoined","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"validMove","type":"uint256"},{"indexed":false,"name":"winnerStatus","type":"uint256"}],"name":"moveEvent","type":"event"}],
  contractAddress : '0x69A2b1Cdd1F7067228EeEc906AbE6148BB429EFb',
  
  init: function() {
    console.log("1");
    return App.initWeb3();
  },





  initWeb3: function() {
    console.log("2");
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
    } else { 
      //use socket or not http provider     
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
    }
    web3 = new Web3(App.web3Provider);
    return App.initContract();
  },








  initContract: function() {
    console.log("3");
      var TicTacToeArtifact;
      TicTacToeArtifact = web3.eth.contract(App.contractABI);
      App.contract = TicTacToeArtifact.at(App.contractAddress);

      console.log("init");
      web3.eth.filter('pending').watch(function (error, log) {App.setListeners();});
      
     
      return App.updateStatus();
      },



  setListeners: function(){
    App.contract.playerJoined().watch(function(err, response){ 
        console.log("join");


        if(response.args.accepted.c[0] == 0){
          console.log("joined");
          App.updateStatus();
        }else{
          console.log("failed to join, game is full");
        }
        $( "#joinp1" ).click(App.handlejoin);
    });




    App.contract.endTurn().watch(function(err, response){ 
        console.log("move");
        web3.eth.getAccounts(function(error, accounts) {
          if (error) {
            console.log(error);
          }
          var account = accounts[0];
          if(response.args.playerAddress == account){
            App.updateStatus(true);
          }else{
            App.updateStatus();
          }
      });
    });

    App.contract.moveEvent().watch(function(err, response){ 
        console.log("moveEvent");
        if(response.args.validMove.c[0] == 1){
          console.log("invalid Move");
        }
        if(response.args.winnerStatus.c[0] == 0){
          console.log("still playing");
        }else if (response.args.winnerStatus.c[0] == 3){
          console.log("its a draw");
        }else{
          console.log("Somebody won!");
        }
        //console.log(response);
    });



  },








  bindEvents: function() {
      $( "td" ).unbind('click');
      $( "#joinp1" ).unbind('click');



      $( "td" ).click(App.handleMove);
      $( "#joinp1" ).click(App.handlejoin);



  },





  handlejoin: function(event) {
    if(event.target.getAttribute("id") == "joinp1"){
      $( "#joinp1" ).unbind('click');
    }
    console.log("join");
    
    event.preventDefault();
    var tictactoeInstance;
    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }
      var account = accounts[0];
      try {
        App.contract.joinGame(account, (function(result){console.log("game joined");}));
      } 
      catch(err) {
        console.log(err.message);
      }
    });
  },







  updateStatus: function(lock) {
    var tictactoeInstance;
    
    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }
      var account = accounts[0];
      try {
        App.contract.getStatus(function(magic, status){console.log("draw");App.draw(magic, status, lock);});
      } 
      catch(err) {
        console.log(err.message);
      }
    });
  },








  draw: function(magic, status,lock) {
    var state = status[0].c[0];
    var board = status[1];
    var addresses = status[2];
    var currPlayer = status[3];

    for (var i = 0, len = board.length; i < len; i++) {
      var val = board[i].c[0];
      var v = "#"+i.toString();
      if(val == 1){
        $( v ).unbind('click');
        $( v ).text("X");

      }else if(val ==2){
        $( v ).unbind('click');
        $( v ).text("O");

      }else{
        $( v ).unbind('click');
        $( v ).click(App.handleMove);
        $( v ).text("");
      }
    }
    if(state == 0){
      //game ready
      $( "h2" ).text("Game ready");
      
    }
    else if(state == 1){
      $( "td" ).unbind('click');
      //$( "#joinp1" ).click(App.handlejoin);
      $( "h2" ).text("X won, Click Join to play again!");
    }
    else if(state == 2){
      $( "td" ).unbind('click');
      //$( "#joinp1" ).click(App.handlejoin);
      $( "h2" ).text("O won, Click Join to play again!");
    }
    else if(state == 3){
      $( "td" ).unbind('click');
      //$( "#joinp1" ).click(App.handlejoin);
      $( "h2" ).text("Draw, Click Join to play again!");

    }
    else if(state == 4){
      $( "h2" ).text("Not ready, Click to join");
      $( "td" ).unbind('click');
    }
    if(lock){
      // $( "td" ).unbind('click');
    }
    if(addresses[0] != "0x0000000000000000000000000000000000000000"){
      $( "#playerId1").text(addresses[0]);
    }else{
      $( "#playerId1").text("Empty");
    }
    if(addresses[1] != "0x0000000000000000000000000000000000000000"){
      $( "#playerId2").text(addresses[1]);
    }else{
      $( "#playerId2").text("Empty");
    }
    if(addresses[0] != "0x0000000000000000000000000000000000000000" && addresses[1] != "0x0000000000000000000000000000000000000000"){
      if(currPlayer == 0){
        $( "#playerTurn1").text("Make your move");
        $( "#playerTurn2").text("");
      }else if(currPlayer == 1){
        $( "#playerTurn2").text("Make your move");
        $( "#playerTurn1").text("");
      }
    }else{
      $( "#playerTurn1").text("");
      $( "#playerTurn2").text("");
    }

  },






  handleMove: function(event) {
    event.preventDefault();
    var tictactoeInstance;
    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }
      var account = accounts[0];
      try {
        var moveInt = parseInt(event.target.getAttribute("id"));
        App.contract.doMove(moveInt, App.handleMoveAns);
      } 
      catch(err) {
        console.log(err.message);
      }
    });
  },

  handleMoveAns: function(event, status, realStatus) {

  }
};




$(function() {
  $(window).load(function() {
    App.init();
    App.bindEvents();
    App.updateStatus();
  });
});


