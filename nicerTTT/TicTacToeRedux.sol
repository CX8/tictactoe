pragma solidity ^0.4.0;
contract TicTacToe {
    uint[] board = new uint[](9);
    address player1;
    address player2;
    address[] players = new address[](2);
    uint playerNum = 0;
    address currentPlayer;
    uint alternator = 0;
    uint winner = 0;
    uint crossOrCircle = 0;
    uint turn = 0;
    
    event endTurn(address playerAddress);

    event playerJoined(uint accepted);

    event moveEvent(uint validMove, uint winnerStatus);

    function joinGame(address sender)public {
        if(winner != 0){
            reset();
        }
        if(playerNum == 0){
           player1 = sender;
           players[0] = player1;
           playerNum  = playerNum + 1;
           playerJoined(0);
       }else if(playerNum == 1){
            player2 = sender;
            players[1] = player2;
            playerNum = playerNum + 1;
            turn = 1;
            switchPlayers();
            playerJoined(0);

       }else{
        playerJoined(1);
       }
    }

    function switchPlayers() public {
        if(crossOrCircle == 0){
                currentPlayer = player1;
            }else{
                currentPlayer = player2;
            }
    }

    function reset() public{
        board = new uint[](9);
        player1 = 0;
        player2 = 0;
        players = new address[](2);
        playerNum = 0;
        winner = 0;
        alternator = alternator + 1;
        crossOrCircle = alternator % 2;
        turn = 0;
        currentPlayer = 0;
        
        //address just because..
        endTurn(msg.sender);
    }
    

    //returning null
    function doMove(uint place) public{
        
        //checks for correct users and correct move
        uint status = 0;
        if(playerNum < 2){
            status = 1;
        }
        if(currentPlayer != msg.sender){
            status = 1;
        }     
        if(place < 0 || place >= 9){
            status = 1;
        }
        if(board[place] != 0){
            status = 1; 
        }
        if(Status == 0){
            board[place] = crossOrCircle + 1;
            crossOrCircle = 1 - crossOrCircle;
            switchPlayers();
            turn = turn + 1;
            winner = checkWinner();
        }
        moveEvent(status, winner);
        //generate event
        endTurn(msg.sender);  
    }
    

    
    uint[][]  tests = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]];
// 0 1 2
// 3 4 5
// 6 7 8
function checkWinner() public constant returns (uint){
    for(uint i =0; i < tests.length;i++){
        uint[] memory currentTest = tests[i];
        if(board[currentTest[0]] == board[currentTest[1]] && board[currentTest[1]] == board[currentTest[2]] && board[currentTest[0]] != 0){
          return board[currentTest[0]];  
        }
    }
    if(turn == 9){
        return 3;
    }

    
    return 0;
}
    //return array of player [player 1 player2] and index of current player
    function getStatus() public constant returns(uint, uint[], address[], uint) {
        uint statusCode;
        if(playerNum < 2){
            statusCode = 4;
        }else{
            statusCode = winner;
        }
        uint[] storage boardStatus = board;
        
        return (statusCode, boardStatus, players, crossOrCircle);
    }

}